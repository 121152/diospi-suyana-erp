<?php

use Illuminate\Support\Str;

return [
    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'sqlsrv'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [
        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true
        ],

        // 'sqlsrv-backup' => [
        //     'driver' => 'sqlsrv',
        //     'url' => env('DATABASE_URL'),
        //     'host' => env('BACK_DB_HOST', 'localhost'),
        //     'port' => env('BACK_DB_PORT', '1433'),
        //     'database' => env('BACK_DB_DATABASE', 'forge'),
        //     'username' => env('BACK_DB_USERNAME', 'forge'),
        //     'password' => env('BACK_DB_PASSWORD', ''),
        //     'charset' => 'utf8',
        //     'prefix' => '',
        //     'prefix_indexes' => true
        // ],

        'pys_vent' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL_PYS_VENT'),
            'host' => env('DB_HOST_PYS_VENT', 'localhost'),
            'port' => env('DB_PORT_PYS_VENT', '1433'),
            'database' => env('DB_DATABASE_PYS_VENT', 'forge'),
            'username' => env('DB_USERNAME_PYS_VENT', 'forge'),
            'password' => env('DB_PASSWORD_PYS_VENT', ''),
            'charset' => 'utf8',
            'collation' => 'SQL_Latin1_General_CP1_CI_AS',
            'prefix' => '',
            'prefix_indexes' => true
        ],

        'pys_segu' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL_PYS_SEGU'),
            'host' => env('DB_HOST_PYS_SEGU', 'localhost'),
            'port' => env('DB_PORT_PYS_SEGU', '1433'),
            'database' => env('DB_DATABASE_PYS_SEGU', 'forge'),
            'username' => env('DB_USERNAME_PYS_SEGU', 'forge'),
            'password' => env('DB_PASSWORD_PYS_SEGU', ''),
            'charset' => 'utf8',
            'collation' => 'SQL_Latin1_General_CP1_CI_AS',
            'prefix' => '',
            'prefix_indexes' => true
        ],

        'pys_logi' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL_PYS_LOGI'),
            'host' => env('DB_HOST_PYS_LOGI', 'localhost'),
            'port' => env('DB_PORT_PYS_LOGI', '1433'),
            'database' => env('DB_DATABASE_PYS_LOGI', 'forge'),
            'username' => env('DB_USERNAME_PYS_LOGI', 'forge'),
            'password' => env('DB_PASSWORD_PYS_LOGI', ''),
            'charset' => 'utf8',
            'collation' => 'SQL_Latin1_General_CP1_CI_AS',
            'prefix' => '',
            'prefix_indexes' => true
        ],

        'hds' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL_HDS'),
            'host' => env('DB_HOST_HDS', '127.0.0.1'),
            'port' => env('DB_PORT_HDS', '3306'),
            'database' => env('DB_DATABASE_HDS', 'forge'),
            'username' => env('DB_USERNAME_HDS', 'forge'),
            'password' => env('DB_PASSWORD_HDS', ''),
            'unix_socket' => env('DB_SOCKET_HDS', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql')
                ? array_filter([
                    PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA')
                ])
                : []
        ]

        // 'sqlite' => [
        //     'driver' => 'sqlite',
        //     'url' => env('DATABASE_URL'),
        //     'database' => env('DB_DATABASE', database_path('database.sqlite')),
        //     'prefix' => '',
        //     'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true)
        // ],

        // 'pgsql' => [
        //     'driver' => 'pgsql',
        //     'url' => env('DATABASE_URL'),
        //     'host' => env('DB_HOST', '127.0.0.1'),
        //     'port' => env('DB_PORT', '5432'),
        //     'database' => env('DB_DATABASE', 'forge'),
        //     'username' => env('DB_USERNAME', 'forge'),
        //     'password' => env('DB_PASSWORD', ''),
        //     'charset' => 'utf8',
        //     'prefix' => '',
        //     'prefix_indexes' => true,
        //     'schema' => 'public',
        //     'sslmode' => 'prefer'
        // ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [
        'client' => env('REDIS_CLIENT', 'phpredis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'redis'),
            'prefix' => env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_') . '_database_')
        ],

        'default' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_DB', '0')
        ],

        'cache' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_CACHE_DB', '1')
        ]
    ]
];
